# Raspberry Anleitung

# Erste Schritte
## SD karte vorbereiten

![Semantic description of image](https://www.raspberrypi.org/homepage-9df4b/static/md-67e1bf35c20ad5893450da28a449efc4.png "Image Title")*Raspberry Pi Imager*[https://www.raspberrypi.org/software/]



Raspberry Image mounter

Wifi config file auf sd karte "wpa_supplicant.conf"
```
country=AT
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
    ssid="NETWORK-NAME"
    psk="NETWORK-PASSWORD"
}
```

# update's
sudo apt-get update
sudo apt-get upgrade

google entfernen
sudo apt-get remove chromium-browser

# Projekte
- DHT22 Temperatur Sensor, [docs](https://www.sparkfun.com/datasheets/Sensors/Temperature/DHT22.pdf)
- waveshare Sense-HAT-B, [docs](https://www.waveshare.com/wiki/Sense_HAT_(B)#Demo_codes)
- pi-hole, [website](https://pi-hole.net/)


## Uninstall program
```
sudo apt-get –purge remove APPNAME
```

# start script with reboot
[tut](https://www.dexterindustries.com/howto/run-a-program-on-your-raspberry-pi-at-startup/)
sudo nano /etc/rc.local

with detached session
```
screen -dm -S <session name> <command>
```

# TV-Projekte

Plex Server
Airplay Server
chromecast Server
